module gitee.com/wenj91/gomini

go 1.23.1

require (
	github.com/apex/log v1.9.0
	github.com/dop251/goja v0.0.0-20240919115326-6c7d1df7ff05
	github.com/efarrer/iothrottler v0.0.3
	github.com/go-errors/errors v1.5.1
	github.com/satori/go.uuid v1.2.0
	github.com/spf13/afero v1.11.0
)

require (
	github.com/dlclark/regexp2 v1.11.4 // indirect
	github.com/go-sourcemap/sourcemap v2.1.4+incompatible // indirect
	github.com/google/pprof v0.0.0-20240910150728-a0b0bb1d4134 // indirect
	github.com/pkg/errors v0.9.1 // indirect
	github.com/rogpeppe/go-internal v1.12.0 // indirect
	golang.org/x/text v0.18.0 // indirect
)
